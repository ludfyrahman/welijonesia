<?php

class M_lijo extends CI_Model{
    
    private $_table = "lijo";

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function create($data)
    {
        $this->db->insert($this->_table, $data);
        return $this->db->insert_id();
    }

}
