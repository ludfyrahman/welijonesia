<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_satuan extends CI_Model
{

    private $_table = "satuan";

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
}

/* End of file M_satuan.php */
