<?php

class M_kategori extends CI_Model{
    
    private $_table = "kategori";

    public $kategori_id;
    public $kategori_nama;
    public $kategori_creat_at;
    public $kategori_creat_by;
    public $kategori_update_at;
    public $kategori_update_by;

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->kategori_id = uniqid();
        $this->kategori_name = $post["nama"];
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->kategori_id = $post["id"];
        $this->kategori_nama = $post["nama"];
    
        return $this->db->update($this->_table, $this, array('kategori_id' => $post['id']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("kategori_id" => $id));
    }

}

?>