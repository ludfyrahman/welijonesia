<?php

class M_komoditas extends CI_Model{
    
    private $_table = "komoditas";

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function create($data)
    {
        $this->db->insert($this->_table, $data);
        return $this->db->insert_id();
    }
}
