<?php

class M_customer extends CI_Model
{

    private $_table = "pengguna";

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function create($data)
    {
        $this->db->insert($this->_table, $data);
        return $this->db->insert_id();
    }

    public function destroy($id)
    {
        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }
}
