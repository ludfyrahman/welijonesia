<?php

function validator($validationRules = [], $data = null)
{
    $CI = &get_instance();
    $CI->load->library('form_validation');

    if (!is_null($data) && !empty($data)) {
        $CI->form_validation->set_data($data);
    }

    $CI->form_validation->set_rules($validationRules);
    return $CI->form_validation->run();
}
