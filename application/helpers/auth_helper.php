<?php

function is_logged()
{
    $CI = &get_instance();

    if ($CI->session->userdata('is_logged') == false && $CI->session->userdata('userName') == "") {
        return false;
    } else {
        return true;
    }
}
