<?php

function convertLevel($level)
{
    if ($level == 1) return "Admin";
    if ($level == 2) return "Lijo";
    return "Customer";
}

function convertDate($date)
{
    $CI = &get_instance();
    $CI->load->helper('date');
    return date("d-m-Y", strtotime($date));
}

function convertTipeLijo($intTipe)
{
    switch ($intTipe) {
        case 1:
            return 'Keliling';
        case 2:
            return 'Mangkal';
        default:
            return 'Campur';
    }
}

function convertStatusPesanan($intStatus)
{
    switch ($intStatus) {
        case 0:
            return 'Dibatalkan';
        case 2:
            return 'Menunggu Pembayaran';
        case 3:
            return 'Diantar';
        default:
            return 'Sampai';
    }
}
