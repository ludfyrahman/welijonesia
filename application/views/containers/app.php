<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="A powerful and conceptual apps base dashboard template that especially build for developers and programmers.">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="<?= base_url('images/favicon.png') ?> ">
    <!-- Page Title  -->
    <title><?= $title ?> | Welijonesia Admin Panel</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="<?= base_url('assets/css/dashlite.css?ver=2.2.0') ?> ">
    <link id="skin-default" rel="stylesheet" href="<?= base_url('assets/css/theme.css?ver=2.2.0') ?> ">
</head>

<body class="nk-body bg-white has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- SIDEBAR -->
            <?php $this->load->view('components/_sidebar') ?>
            <!-- END SIDEBAR -->
            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- HEADER -->
                <?php $this->load->view('components/_header') ?>
                <!-- END HEADER -->
                <!-- HEADER -->
                <?php $this->load->view('pages/' . $page . '/content') ?>
                <!-- END HEADER -->
                <!-- FOOTER -->
                <?php $this->load->view('components/_footer') ?>
                <!-- END FOOTER -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
    <!-- JavaScript -->
    <script src="<?= base_url('assets/js/bundle.js?ver=2.2.0') ?> "></script>
    <script src="<?= base_url('assets/js/scripts.js?ver=2.2.0') ?> "></script>
    <script src="<?= base_url('assets/js/charts/gd-default.js?ver=2.2.0') ?> "></script>
    <?php $this->load->view('pages/' . $page . '/js') ?>
</body>

</html>