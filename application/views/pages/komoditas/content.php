<!-- content @s -->
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-lg">
        <div class="nk-content-body">
            <div class="components-preview wide-md mx-auto">
                <div class="nk-block nk-block-lg">
                    <div class="nk-block-head">
                        <div class="nk-block-head-content">
                            <h4 class="nk-block-title">Daftar Komoditas</h4>
                            <div class="nk-block-des">
                                <p>Terdapat <?= count($model) ?> Komoditas Terdaftar.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card card-preview">
                        <div class="card-inner">
                            <table class="datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="false">
                                <thead>
                                    <tr class="nk-tb-item nk-tb-head">
                                        <th class="nk-tb-col nk-tb-col-check">
                                            <div class="custom-control custom-control-sm custom-checkbox notext">
                                                <input type="checkbox" class="custom-control-input" id="uid">
                                                <label class="custom-control-label" for="uid"></label>
                                            </div>
                                        </th>
                                        <th class="nk-tb-col"><span class="sub-text">Komoditas</span></th>
                                        <th class="nk-tb-col tb-col-mb text-center"><span class="sub-text">Kategori</span></th>
                                        <th class="nk-tb-col tb-col-md text-center"><span class="sub-text">Harga</span></th>
                                        <th class="nk-tb-col tb-col-lg text-center"><span class="sub-text">Satuan</span></th>
                                        <th class="nk-tb-col nk-tb-col-tools text-right">
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php foreach ($model as $komoditas) : ?>
                                        <tr class="nk-tb-item">
                                            <td class="nk-tb-col nk-tb-col-check">
                                                <div class="custom-control custom-control-sm custom-checkbox notext">
                                                    <input type="checkbox" class="custom-control-input" id="uid<?= $komoditas['id'] ?>">
                                                    <label class="custom-control-label" for="uid<?= $komoditas['id'] ?>"></label>
                                                </div>
                                            </td>
                                            <td class="nk-tb-col">
                                                <div class="user-card">
                                                    <div class="user-avatar bg-dim-primary d-none d-sm-flex">
                                                        <span>X</span>
                                                    </div>
                                                    <div class="user-info">
                                                        <span class="tb-lead"><?= $komoditas['nama'] ?> <span class="dot dot-success d-md-none ml-1"></span></span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="nk-tb-col tb-col-mb text-center" data-order="35040.34">
                                                <span><?= $komoditas['kategori'] ?></span>
                                            </td>
                                            <td class="nk-tb-col tb-col-md text-center">
                                                <span>Rp <?= $komoditas['harga'] ?></span>
                                            </td>
                                            <td class="nk-tb-col tb-col-lg text-center" data-order="Email Verified - Kyc Unverified">
                                                <span><?= $komoditas['satuan'] ?></span>
                                            </td>
                                            <td class="nk-tb-col nk-tb-col-tools">
                                                <ul class="nk-tb-actions gx-1">
                                                    <li>
                                                        <div class="dropdown">
                                                            <a class="text-soft dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown" aria-expanded="false"><em class="icon ni ni-more-h"></em></a>
                                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-xs" style="">
                                                                <ul class="link-list-plain">
                                                                    <li><a href="#">View</a></li>
                                                                    <li><a href="#">Edit</a></li>
                                                                    <li><a href="#">Remove</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr><!-- .nk-tb-item  -->
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div><!-- .card-preview -->
                </div> <!-- nk-block -->
            </div><!-- .components-preview -->
        </div>
    </div>
</div>
<!-- content @e -->