<!-- content @s -->
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-lg">
        <div class="nk-content-body">
            <div class="components-preview wide-md mx-auto">
                <div class="nk-block-head nk-block-head-lg wide-sm">
                    <div class="nk-block-head-content">
                        <h2 class="nk-block-title fw-normal">Tambah Pengguna</h2>
                    </div>
                </div><!-- .nk-block-head -->
                <?php $this->load->view('components/_error') ?>
                </br>
                <div class="nk-block nk-block-lg">
                    <div class="card card-bordered">
                        <div class="card-inner">
                            <form action="<?= base_url(uri_string()) ?>" class="form-validate is-alter" method="post">
                                <div class="row g-gs">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="fva-full-name">Nama Lengkap</label>
                                            <div class="form-control-wrap">
                                                <input type="text" class="form-control" name="nama" id="fva-full-name" value="<?= $input['nama'] ?>" required>
                                            </div>
                                        </div>
                                        <?= form_error('nama', '<span class="text-danger">', '</span>'); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="fva-email">Alamat Email</label>
                                            <div class="form-control-wrap">
                                                <input type="email" class="form-control" name="email" id="fva-email" value="<?= $input['email'] ?>" required>
                                            </div>
                                        </div>
                                        <?= form_error('email', '<span class="text-danger">', '</span>'); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="fva-telepon">Telepon</label>
                                            <div class="form-control-wrap">
                                                <input type="tel" class="form-control" name="telepon" id="fva-telepon" value="<?= $input['telepon'] ?>" required>
                                            </div>
                                        </div>
                                        <?= form_error('telepon', '<span class="text-danger">', '</span>'); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="fva-whatsapp">Whatsapp (optional)</label>
                                            <div class="form-control-wrap">
                                                <input type="tel" class="form-control" name="whatsapp" value="<?= $input['whatsapp'] ?>" id="fva-telepon">
                                            </div>
                                        </div>
                                        <?= form_error('whatsapp', '<span class="text-danger">', '</span>'); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-gender" for="fva-gender">Jenis Kelamin</label>
                                            <div class="form-control-wrap ">
                                                <select class="form-control form-select" id="fva-gender" name="jenis_kelamin" data-placeholder="Pilih Jenis Kelamin" required>
                                                    <option label="empty" value=""></option>
                                                    <option value="1">Laki-Laki</option>
                                                    <option value="0">Perempuan</option>
                                                </select>
                                            </div>
                                        </div>
                                        <?= form_error('jenis_kelamin', '<span class="text-danger">', '</span>'); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="fva-level">Level</label>
                                            <div class="form-control-wrap ">
                                                <select class="form-control form-select" id="fva-level" name="level" data-placeholder="Pilih Level Pengguna" required>
                                                    <option label="empty" value=""></option>
                                                    <option value="1">Admin</option>
                                                    <option value="2">Lijo</option>
                                                    <option value="3">Pengguna</option>
                                                </select>
                                            </div>
                                        </div>
                                        <?= form_error('level', '<span class="text-danger">', '</span>'); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="fw-password">Password</label>
                                            <div class="form-control-wrap">
                                                <input type="password" data-msg="Required" name="password" class="form-control required" id="fw-password" required>
                                            </div>
                                        </div>
                                        <?= form_error('password', '<span class="text-danger">', '</span>'); ?>
                                    </div><!-- .col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="fw-re-password">Konfirmasi Password</label>
                                            <div class="form-control-wrap">
                                                <input type="password" data-msg="Required" class="form-control required" id="fw-re-password" name="konfirmasi_password" required>
                                            </div>
                                        </div>
                                        <?= form_error('konfirmasi_password', '<span class="text-danger">', '</span>'); ?>
                                    </div><!-- .col -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-lg btn-primary">Tambahkan</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- .nk-block -->
            </div><!-- .components-preview -->
        </div>
    </div>
</div>
<!-- content @e -->