<!-- content @s -->
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-lg">
        <div class="nk-content-body">
            <div class="components-preview wide-md mx-auto">
                <div class="nk-block-head nk-block-head-lg wide-sm">
                    <div class="nk-block-head-content">
                        <h2 class="nk-block-title fw-normal">Tambah Lijo</h2>
                    </div>
                </div><!-- .nk-block-head -->
                <?php $this->load->view('components/_error') ?>
                </br>
                <div class="nk-block nk-block-lg">
                    <div class="card card-bordered">
                        <div class="card-inner">
                            <form action="<?= base_url(uri_string()) ?>" class="form-validate is-alter" method="post">
                                <div class="row g-gs">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="fva-full-name">Nama Lijo</label>
                                            <div class="form-control-wrap">
                                                <input type="text" class="form-control" name="nama" id="fva-full-name" value="<?= $input['nama'] ?? '' ?>" required>
                                            </div>
                                        </div>
                                        <?= form_error('nama', '<span class="text-danger">', '</span>'); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="fva-pengguna">Pemilik</label>
                                            <div class="form-control-wrap ">
                                                <select class="form-control form-select" id="fva-pengguna" name="pengguna" data-placeholder="Pilih Pemilik Lijo" required>
                                                    <option label="empty" value=""></option>
                                                    <?php foreach ($data['pengguna'] as $value) : ?>
                                                        <option value="<?= $value->id ?>"><?= $value->nama ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?= form_error('pengguna', '<span class="text-danger">', '</span>'); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="fva-tipe">Tipe</label>
                                            <div class="form-control-wrap ">
                                                <select class="form-control form-select" id="fva-tipe" name="tipe" data-placeholder="Pilih Tipe Lijo" required>
                                                    <option label="empty" value=""></option>
                                                    <option value="1">Keliling</option>
                                                    <option value="2">Mangkal</option>
                                                    <option value="3">Campur</option>
                                                </select>
                                            </div>
                                        </div>
                                        <?= form_error('tipe', '<span class="text-danger">', '</span>'); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="nk-upload-form">
                                            <label class="form-label" for="fva-photo">Photo</label>
                                            <div class="upload-zone small bg-lighter">
                                                <span class="dz-message-text"><span>Drag and drop</span> file here or <span>browse</span></span>
                                                <div class="dz-message" data-dz-message>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-lg btn-primary">Tambahkan</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- .nk-block -->
            </div><!-- .components-preview -->
        </div>
    </div>
</div>
<!-- content @e -->