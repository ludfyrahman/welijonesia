<!-- main @s -->
<div class="nk-main ">
    <!-- wrap @s -->
    <div class="nk-wrap nk-wrap-nosidebar">
        <!-- content @s -->
        <div class="nk-content ">
            <div class="nk-block nk-block-middle nk-auth-body  wide-xs">
                <div class="brand-logo pb-4 text-center">
                    <a href="html/index.html" class="logo-link">
                        <img class="logo-light logo-img logo-img-lg" src="<?= base_url('assets/images/logo.png') ?>" srcset="./images/logo2x.png 2x" alt="logo">
                        <img class="logo-dark logo-img logo-img-lg" src="<?= base_url('assets/images/logo-dark.png') ?>" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                    </a>
                </div>
                <div class="card card-bordered">
                    <div class="card-inner card-inner-lg">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="nk-block-title">Sign-In</h4>
                                <div class="nk-block-des">
                                    <p>Access the Admin panel using your email and password.</p>
                                </div>
                            </div>
                        </div>
                        <?php $this->load->view('components/_error') ?>
                        <br />
                        <form action="<?= base_url(uri_string()) ?>" method="post">
                            <div class="form-group">
                                <div class="form-label-group">
                                    <label class="form-label" for="default-01">Email</label>
                                </div>
                                <input type="text" class="form-control form-control-lg" name="email" value="<?= $input['email'] ?? '' ?>" id="default-01" placeholder="Enter your email address or username">
                                <?= form_error('email') ?>
                            </div>
                            <div class="form-group">
                                <div class="form-label-group">
                                    <label class="form-label" for="password">Password</label>
                                </div>
                                <div class="form-control-wrap">
                                    <a href="#" class="form-icon form-icon-right passcode-switch" data-target="password">
                                        <em class="passcode-icon icon-show icon ni ni-eye"></em>
                                        <em class="passcode-icon icon-hide icon ni ni-eye-off"></em>
                                    </a>
                                    <input type="password" class="form-control form-control-lg" name="password" id="password" placeholder="Enter your passcode">
                                </div>
                                <?= form_error('password') ?>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-lg btn-primary btn-block">Sign in</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="nk-footer nk-auth-footer-full">
                <div class="container wide-lg">
                    <div class="row g-3">
                        <div class="col-lg-12 ">
                            <div class="nk-block-content text-center text-lg-left">
                                <p class="text-soft text-center">&copy; 2020 Welijonesia. All Rights Reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- wrap @e -->
    </div>
    <!-- content @e -->
</div>
<!-- main @e -->