<!-- footer @s -->
<div class="nk-footer">
    <div class="container-fluid">
        <div class="nk-footer-wrap">
            <div class="nk-footer-copyright"> &copy; 2021 Welijonesia. Dikembangkan oleh <a href="https://stack.co.id" target="_blank">Team Dev Welijonesia</a>
            </div>
        </div>
    </div>
</div>
<!-- footer @e -->