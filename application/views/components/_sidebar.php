<!-- sidebar @s -->
<div class="nk-sidebar nk-sidebar-fixed " data-content="sidebarMenu">
    <div class="nk-sidebar-element nk-sidebar-head">
        <div class="nk-sidebar-brand">
            <a href="<?= base_url('admin/html/index.html') ?>" class="logo-link nk-sidebar-logo">
                <img class="logo-light logo-img" src="<?= base_url('assets/images/logo.png') ?> " srcset="<?= base_url('assets/images/logo2x.png 2x') ?> " alt="logo">
                <img class="logo-dark logo-img" src="<?= base_url('assets/images/logo-dark.png') ?> " srcset="<?= base_url('assets/images/logo-dark2x.png 2x') ?> " alt="logo-dark">
            </a>
        </div>
        <div class="nk-menu-trigger mr-n2">
            <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
        </div>
    </div><!-- .nk-sidebar-element -->
    <div class="nk-sidebar-element">
        <div class="nk-sidebar-body" data-simplebar>
            <div class="nk-sidebar-content">
                <div class="nk-sidebar-menu">
                    <ul class="nk-menu">
                        <li class="nk-menu-heading">
                            <h6 class="overline-title text-primary-alt">Dashboards</h6>
                        </li><!-- .nk-menu-item -->
                        <li class="nk-menu-item">
                            <a href="<?= base_url('admin/html/index-sales.html') ?>" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-cc-alt2"></em></span>
                                <span class="nk-menu-text">Sales Dashboard</span>
                            </a>
                        </li><!-- .nk-menu-item -->
                        <li class="nk-menu-item">
                            <a href="<?= base_url('admin/html/index-analytics.html') ?>" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-growth"></em></span>
                                <span class="nk-menu-text">Analytics Dashboard</span>
                            </a>
                        </li><!-- .nk-menu-item -->
                        <li class="nk-menu-heading">
                            <h6 class="overline-title text-primary-alt">DATA MASTER</h6>
                        </li><!-- .nk-menu-heading -->
                        <li class="nk-menu-item has-sub">
                            <a href="#" class="nk-menu-link nk-menu-toggle">
                                <span class="nk-menu-icon"><em class="icon ni ni-users"></em></span>
                                <span class="nk-menu-text">Pengguna</span>
                            </a>
                            <ul class="nk-menu-sub">
                                <li class="nk-menu-item">
                                    <a href="<?= base_url('admin/pengguna') ?>" class="nk-menu-link"><span class="nk-menu-text">Daftar Pengguna</span></a>
                                </li>
                                <li class="nk-menu-item">
                                    <a href="<?= base_url('admin/pengguna/tambah') ?>" class="nk-menu-link"><span class="nk-menu-text">Tambah Pengguna</span></a>
                                </li>
                            </ul><!-- .nk-menu-sub -->
                        </li><!-- .nk-menu-item -->
                        <li class="nk-menu-item">
                            <a href="<?= base_url('admin/pesanan') ?>" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-cc-alt2"></em></span>
                                <span class="nk-menu-text">Pesanan</span>
                            </a>
                        </li><!-- .nk-menu-item -->

                        <li class="nk-menu-item has-sub">
                            <a href="#" class="nk-menu-link nk-menu-toggle">
                                <span class="nk-menu-icon"><em class="icon ni ni-tile-thumb"></em></span>
                                <span class="nk-menu-text">Komoditas</span>
                            </a>
                            <ul class="nk-menu-sub">
                                <li class="nk-menu-item">
                                    <a href="<?= base_url('admin/komoditas') ?>" class="nk-menu-link"><span class="nk-menu-text">Daftar Komoditas</span></a>
                                </li>
                                <li class="nk-menu-item">
                                    <a href="<?= base_url('admin/komoditas/tambah') ?>" class="nk-menu-link"><span class="nk-menu-text">Tambah Komoditas</span></a>
                                </li>
                            </ul><!-- .nk-menu-sub -->
                        </li><!-- .nk-menu-item -->
                        <li class="nk-menu-item has-sub">
                            <a href="#" class="nk-menu-link nk-menu-toggle">
                                <span class="nk-menu-icon"><em class="icon ni ni-grid-alt"></em></span>
                                <span class="nk-menu-text">Lijo</span>
                            </a>
                            <ul class="nk-menu-sub">
                                <li class="nk-menu-item">
                                    <a href="<?= base_url('admin/lijo') ?>" class="nk-menu-link"><span class="nk-menu-text">Daftar Lijo</span></a>
                                </li>
                                <li class="nk-menu-item">
                                    <a href="<?= base_url('admin/lijo/tambah') ?>" class="nk-menu-link"><span class="nk-menu-text">Tambah Lijo</span></a>
                                </li>

                            </ul><!-- .nk-menu-sub -->
                        </li><!-- .nk-menu-item -->
                        <li class="nk-menu-heading">
                            <h6 class="overline-title text-primary-alt">PENGATURAN</h6>
                        </li><!-- .nk-menu-heading -->
                        <li class="nk-menu-item">
                            <a href="<?= base_url('admin/pengaturan') ?>" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-growth"></em></span>
                                <span class="nk-menu-text">Aplikasi</span>
                            </a>
                        </li><!-- .nk-menu-item -->

                    </ul><!-- .nk-menu -->
                </div><!-- .nk-sidebar-menu -->
            </div><!-- .nk-sidebar-content -->
        </div><!-- .nk-sidebar-body -->
    </div><!-- .nk-sidebar-element -->
</div>
<!-- sidebar @e -->