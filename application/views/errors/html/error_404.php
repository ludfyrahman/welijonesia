<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>

<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
	<meta charset="utf-8">
	<meta name="author" content="Softnio">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="A powerful and conceptual apps base dashboard template that especially build for developers and programmers.">
	<!-- Fav Icon  -->
	<link rel="shortcut icon" href="<?= config_item('base_url') . 'assets/images/favicon.png' ?>">
	<!-- Page Title  -->
	<title>Error 404 | DashLite Admin Template</title>
	<!-- StyleSheets  -->
	<link rel="stylesheet" href="<?= config_item('base_url') . 'assets/css/dashlite.css?ver=2.2.0' ?>">
	<link id="skin-default" rel="stylesheet" href="<?= config_item('base_url') . 'assets/css/theme.css?ver=2.2.0' ?>">
</head>

<body class="nk-body bg-white npc-general pg-error">
	<div class="nk-app-root">
		<!-- main @s -->
		<div class="nk-main ">
			<!-- wrap @s -->
			<div class="nk-wrap nk-wrap-nosidebar">
				<!-- content @s -->
				<div class="nk-content ">
					<div class="nk-block nk-block-middle wide-md mx-auto">
						<div class="nk-block-content nk-error-ld text-center">
							<img class="nk-error-gfx" src="<?= config_item('base_url') . 'assets/images/gfx/error-404.svg' ?>" alt="">
							<div class="wide-xs mx-auto">
								<h3 class="nk-error-title">Oops! Kenapa kamu disini?</h3>
								<p class="nk-error-text">Mohon maaf sepertinya halaman yang anda akses tidak tersedia.</p>
								<a href="<?= config_item('base_url') ?>" class="btn btn-lg btn-primary mt-2">Kembali ke Halaman Utama</a>
							</div>
						</div>
					</div><!-- .nk-block -->
				</div>
				<!-- wrap @e -->
			</div>
			<!-- content @e -->
		</div>
		<!-- main @e -->
	</div>
	<!-- app-root @e -->
	<!-- JavaScript -->
	<script src="<?= config_item('base_url') . 'assets/js/bundle.js?ver=2.2.0' ?>"></script>
	<script src="<?= config_item('base_url') . 'assets/js/scripts.js?ver=2.2.0' ?>"></script>

</html>