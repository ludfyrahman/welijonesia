<?php 
class Pesanan extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->table = "pemesanan";
    }
    public function tambah($id_pengguna){
        $response = [];
        $response['status'] = false;
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $p = $_POST;
            $d = $this->db->get_where("keranjang",['id_pengguna' => $id_pengguna]);
            $cekPesanan = $this->db->query("SELECT * FROM pemesanan WHERE id_pengguna = '$id_pengguna' AND status!=3 AND status !=0")->num_rows();
            $now = date("Y-m-d H:i:s");
            $limit = date("Y-m-d")." 13:00:00";
            $max = date("Y-m-d")." 21:00:00";
            if($now > $limit && $now < $max){
                if($cekPesanan < 1){
                    $data = $d->result_array();
                    $row = $d->row_array();
                    $id_lijo = $row['id_lijo'];
                    $arr = [
                        'id_pengguna' => $id_pengguna,
                        'tanggal_expired' => date("Y-m-d H:i:s", strtotime("+1 days")),
                        'id_lijo' => $id_lijo,
                        'id_alamat' => $p['id_alamat']
                    ];
                    $lijo = $this->db->get_where("lijo", ['id' => $id_lijo]);
                    $this->db->insert($this->table, $arr);
                    $id = $this->db->insert_id();
                    $this->db->insert("notifikasi", ['title' => 'Pemesanan Telah masuk ke lijo', 'description' => 'Pesanan akan diantarkan besok di jam 05:00 - 10:00 pagi', 'id_pengguna' => $id_pengguna, 'id_pemesanan' => $id]);
                    $this->db->insert("notifikasi", ['title' => 'Pemesanan Baru', 'description' => 'Ada pesanan baru BOS', 'id_pengguna' => $id_lijo, 'id_pemesanan' => $id, 'penerima' => 1]);
                foreach ($data as $dd) {
                        $detail = [
                            'id_detail_produk' => $dd['id_detail_produk'],
                            'id_pemesanan' => $id,
                            'jumlah' => $dd['jumlah'],
                            'subtotal' => $dd['subtotal'],
                        ];
                        $this->db->insert("detail_pemesanan", $detail);
                }
                $this->db->delete("keranjang", ['id_pengguna' => $id_pengguna]);
                $response['pesan'] = 'Pesanan Berhasil Dibuat';
                $response['status'] = true;
                }else{
                    $response['pesan'] = 'Anda masih memiliki pesanan yang belum selesai';
                    $response['status'] = false;
                }
            }else{
                $response['pesan'] = 'Pesanan Tidak bisa untuk sekarang. Terakhir Pemesanan Jam 21:00';
                $response['status'] = false;
            }
        }else{
            $response['pesan'] = 'Method Not allowed';
            $response['status'] = false;
        }
        echo json_encode($response);
    }
    public function batalkan($id){
        $pemesanan = $this->db->get_where($this->table, ['id_pengguna' => $id, 'status' => 1])->row_array();
        $now = date("Y-m-d H:i:s");
        $limit = date("Y-m-d")." 13:00:00";
        $max = date("Y-m-d")." 23:00:00";
        if($now > $limit && $now < $max){
            // echo "lebih besar now";
            $date = date("Y-m-d H:i:s");
            $lijo = $this->db->get_where("lijo", ['id' => $pemesanan['id_lijo']])->row_array();
            $q = $this->db->update("$this->table", ['status' => 0], ['id_pengguna' => $pemesanan['id_pengguna']]);
            $cek = $this->db->query("SELECT pg.*, p.id FROM pemesanan p JOIN pengguna pg ON p.id_pengguna=pg.id WHERE p.id_pengguna = '$id' and p.status='0'")->row_array();
            $this->db->insert("notifikasi", ['title' => 'Pemesanan Dibatalkan', 'description' => 'Pemesanan atas nama '.$cek['nama']." dibatalkan pada ".$date, 'id_pengguna' => $pemesanan['id_pengguna'], 'id_pemesanan' => $cek['id'], 'penerima' => '2']);
            $this->db->insert("notifikasi", ['title' => 'Pemesanan Berhasil Dibatalkan', 'description' => 'Pemesanan anda '.KODE_INVOICE.$cek['id']." dibatalkan pada ".$date, 'id_pengguna' => $lijo['id_pengguna'], 'id_pemesanan' => $cek['id'], 'penerima' => '1']);
            if($q){
                $response['pesan'] = 'Pesanan Berhasil Dibatalkan ';
                $response['status'] = true;
            }else{
                $response['pesan'] = 'Pesanan Gagal Dibatalkan';
                $response['status'] = false;
            }
        }else{
            $response['pesan'] = 'Pesanan Tidak bisa dibatalkan karena sudah melebihi batas waktu';
            $response['status'] = false;
        }
        echo json_encode($response);
    }
    public function antarkan($id){
        $pemesanan = $this->db->get_where($this->table, ['id' => $id])->row_array();
        $now = date("Y-m-d H:i:s");
        $limit = date("Y-m-d")." 13:00:00";
        // if($now > $limit){
            // echo "lebih besar now";
            $q = $this->db->update("$this->table", ['status' => 2], ['id' => $id]);
            $lijo = $this->db->get_where("lijo", ['id' => $pemesanan['id_lijo']])->row_array();
            $cek = $this->db->query("SELECT pg.*, p.id FROM pemesanan p JOIN pengguna pg ON p.id_pengguna=pg.id WHERE p.id_pengguna = '$pemesanan[id_pengguna]' and p.status='0'")->row_array();
            $this->db->insert("notifikasi", ['title' => 'Pemesanan berhasil Diantarkan', 'penerima' => 1, 'description' => 'Pemesanan atas nama '.$cek['nama']." diantarkan pada ".$now, 'id_pengguna' => $lijo['id_pengguna'], 'id_pemesanan' => $cek['id']]);
            $this->db->insert("notifikasi", ['title' => 'Pesanan dalam perjalanan', 'penerima' => 2, 'description' =>  'Pemesanan anda '.KODE_INVOICE.$cek['id'].' masih diantarkan', 'id_pengguna' => $pemesanan['id_pengguna'], 'id_pemesanan' => $cek['id']]);
            if($q){
                $response['pesan'] = 'Pesanan Proses Diantarkan';
                $response['status'] = true;
            }else{
                $response['pesan'] = 'Pesanan Gagal Proses Diantarkan';
                $response['status'] = false;
            }
        // }else{
        //     $response['pesan'] = 'Pesanan Tidak bisa dibatalkan karena sudah melebihi batas waktu';
        //     $response['status'] = false;
        // }
        echo json_encode($response);
    }
    public function sampai($id){
        $pemesanan = $this->db->get_where($this->table, ['id' => $id])->row_array();
        $now = date("Y-m-d H:i:s");
        $limit = date("Y-m-d")." 13:00:00";
        // if($now > $limit){
            // echo "lebih besar now";
            $lijo = $this->db->get_where("lijo", ['id' => $pemesanan['id_lijo']])->row_array();
            $q = $this->db->update("$this->table", ['status' => 3], ['id' => $id]);
            $cek = $this->db->query("SELECT pg.*, p.id FROM pemesanan p JOIN pengguna pg ON p.id_pengguna=pg.id WHERE p.id_pengguna = '$pemesanan[id_pengguna]' and p.status='0'")->row_array();
            $this->db->insert("notifikasi", ['title' => 'Pemesanan Telah sampai', 'penerima' => 1, 'description' => 'Pemesanan atas nama '.$cek['nama']." diantarkan pada ".$now, 'id_pengguna' => $lijo['id_pengguna'], 'id_pemesanan' => $cek['id']]);
            $this->db->insert("notifikasi", ['title' => 'Pemesanan Telah sampai', 'penerima' => 2, 'description' =>  'Pemesanan anda '.KODE_INVOICE.$cek['id'].' telah sampai', 'id_pengguna' => $pemesanan['id_pengguna'], 'id_pemesanan' => $cek['id']]);
            if($q){
                $response['pesan'] = 'Pesanan Berhasil Diantarkan';
                $response['status'] = true;
            }else{
                $response['pesan'] = 'Pesanan Gagal Diantarkan';
                $response['status'] = false;
            }
        // }else{
        //     $response['pesan'] = 'Pesanan Tidak bisa dibatalkan karena sudah melebihi batas waktu';
        //     $response['status'] = false;
        // }
        echo json_encode($response);
    }
    public function dataPesanan($id){
        $data['data'] = $this->db->query("SELECT p.*, l.foto, l.id as id_lijo, l.nama, p.status, (CASE p.status WHEN 1 THEN 'PENDING' WHEN 2 THEN 'DIANTAR' WHEN 3 THEN 'SELESAI' ELSE 'DIBATALKAN' END) as  status_text FROM `pemesanan` p 
        JOIN lijo l ON p.id_lijo=l.id 
        JOIN alamat a ON p.id_alamat=a.id WHERE p.id_pengguna=$id and p.status!=3 AND p.status!=0")->row_array();
        $data['pesanan'] = $this->db->query("SELECT kom.nama, pg.telepon, dpro.harga, s.nama as satuan, pem.id, dp.jumlah, dp.subtotal, (CASE WHEN p.foto IS NULL THEN kom.foto ELSE p.foto END) as foto        
        FROM detail_pemesanan dp 
        JOIN detail_produk dpro ON dp.id_detail_produk=dpro.id
        JOIN produk p ON dpro.id_produk=p.id 
        JOIN pemesanan pem ON pem.id=dp.id_pemesanan
        LEFT JOIN pengguna pg ON pem.id_pengguna=pg.id
        JOIN komoditas kom ON p.id_komoditas=kom.id
        JOIN satuan s ON dpro.id_satuan=s.id
        WHERE pem.id_pengguna=$id and pem.status!=3 AND pem.status!=0")->result_array();
        echo json_encode($data);
    }
    // pesanan di lijo
    public function data($id_pengguna, $st= null){
        // $data['lijo'] = $this->db->query("SELECT l.* FROM $this->table k LEFT JOIN lijo l ON k.id_lijo=l.id  WHERE k.id_pengguna=$id")->row_array();
        $status = ($st == null ? "AND (p.status != 3 AND p.status!=0 )" : '');
        $data['pesanan'] = $this->db->query("SELECT p.id, a.alamat, pg.nama, (CASE p.status WHEN 1 THEN 'PENDING' WHEN 2 THEN 'DIANTAR' WHEN 3 THEN 'SELESAI' ELSE  'BATAL' END)status, pg.telepon, SUM(dp.subtotal) total FROM pemesanan p 
        LEFT JOIN pengguna pg ON p.id_pengguna=pg.id
        LEFT JOIN detail_pemesanan dp ON p.id=dp.id_pemesanan 
        LEFT JOIN alamat a ON p.id_alamat=a.id 
        LEFT JOIN lijo l ON p.id_lijo=l.id
        WHERE l.id_pengguna=$id_pengguna $status GROUP BY dp.id_pemesanan ORDER BY p.id DESC")->result_array();
        $data['rekap'] = $this->db->query("SELECT p.id, a.alamat, pg.nama, pg.telepon, SUM(dp.subtotal) total FROM pemesanan p JOIN pengguna pg ON p.id_pengguna=p.id JOIN detail_pemesanan dp ON p.id=dp.id_pemesanan JOIN alamat a ON p.id_alamat=a.id WHERE pg.id=$id_pengguna and p.status=1 GROUP BY dp.id_pemesanan")->result_array();
        echo json_encode($data);
    }
    public function dataPesananCustomer($id_pengguna){
        // $data['lijo'] = $this->db->query("SELECT l.* FROM $this->table k LEFT JOIN lijo l ON k.id_lijo=l.id  WHERE k.id_pengguna=$id")->row_array();
        $data['pesanan'] = $this->db->query("SELECT p.id, a.alamat, pg.nama, p.status as st, (CASE p.status WHEN 1 THEN 'PENDING' WHEN 2 THEN 'DIANTAR' WHEN 3 THEN 'SELESAI'  ELSE  'BATAL' END)status, pg.telepon, SUM(dp.subtotal) total FROM pemesanan p 
        LEFT JOIN pengguna pg ON p.id_pengguna=pg.id
        LEFT JOIN detail_pemesanan dp ON p.id=dp.id_pemesanan 
        LEFT JOIN alamat a ON p.id_alamat=a.id 
        WHERE pg.id=$id_pengguna GROUP BY dp.id_pemesanan")->result_array();
        $data['rekap'] = $this->db->query("SELECT p.id, a.alamat, pg.nama, pg.telepon, SUM(dp.subtotal) total FROM pemesanan p JOIN pengguna pg ON p.id_pengguna=p.id JOIN detail_pemesanan dp ON p.id=dp.id_pemesanan JOIN alamat a ON p.id_alamat=a.id WHERE pg.id=$id_pengguna and p.status=1 GROUP BY dp.id_pemesanan")->result_array();
        echo json_encode($data);
    }
    public function detail_pesanan($id){
        $data['data'] = $this->db->query("SELECT p.id, p.status, a.alamat, pg.nama, pg.telepon, pg.foto, SUM(dp.subtotal) total, p.status, (CASE p.status WHEN 1 THEN 'PENDING' WHEN 2 THEN 'DIANTAR' WHEN 3 THEN 'SELESAI'  ELSE  'BATAL' END) as  status_text FROM pemesanan p 
        JOIN pengguna pg ON p.id_pengguna=pg.id
        JOIN detail_pemesanan dp ON p.id=dp.id_pemesanan 
        JOIN alamat a ON p.id_alamat=a.id 
        JOIN lijo l ON p.id_lijo=l.id
        
        WHERE p.id=$id GROUP BY dp.id_pemesanan")->row_array();
        $data['produk'] = $this->db->query("SELECT kom.nama, dpro.harga, s.nama as satuan, pem.id, dp.jumlah, dp.subtotal, (CASE WHEN p.foto IS NULL THEN kom.foto ELSE p.foto END) as foto  FROM  detail_pemesanan dp 
        JOIN detail_produk dpro ON dp.id_detail_produk=dpro.id
        JOIN produk p ON dpro.id_produk=p.id 
        JOIN pemesanan pem ON pem.id=dp.id_pemesanan
        JOIN komoditas kom ON p.id_komoditas=kom.id
        JOIN satuan s ON dpro.id_satuan=s.id WHERE pem.id=$id")->result_array();
        echo json_encode($data);
    }
    public function detail($id){
        $data['data'] = $this->db->query("SELECT p.*, dp.jumlah, dp.subtotal, k.nama, s.nama as satuan FROM `detail_pemesanan` dp 
        JOIN pemesanan pem ON dp.id_pemesanan=pem.id 
        JOIN detail_produk dpro ON dp.id_detail_produk=dpro.id 
        JOIN produk p ON dpro.id_produk=p.id 
        JOIN komoditas k ON p.id_komoditas=k.id
        JOIN satuan s ON dpro.id_satuan=s.id
        WHERE pem.id_pengguna='$id'")->result_array();
        echo json_encode($data);
    }
    public function map($id){
        $data['data'] = $this->db->query("SELECT a.* from pemesanan p JOIN lijo l ON p.id_lijo=l.id JOIN pengguna pg ON l.id_pengguna=pg.id JOIN alamat a ON p.id_alamat=a.id WHERE pg.id=$id")->result_array();
        echo json_encode($data);
    }
}
