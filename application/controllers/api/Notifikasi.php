<?php 
class Notifikasi extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->table = "notifikasi";
    }
    // list data notifikasi customer
    public function data($id_pengguna){
        // $data['lijo'] = $this->db->query("SELECT l.* FROM $this->table k LEFT JOIN lijo l ON k.id_lijo=l.id  WHERE k.id_pengguna=$id")->row_array();
        $data['data'] = $this->db->query("SELECT * FROM $this->table WHERE id_pengguna = '$id_pengguna' AND  penerima = '2' ORDER BY id desc")->result_array();
        echo json_encode($data);
    }
    // list data notifikasi lijo
    public function dataNotifikasi($id_pengguna = null){
        // $data['lijo'] = $this->db->query("SELECT l.* FROM $this->table k LEFT JOIN lijo l ON k.id_lijo=l.id  WHERE k.id_pengguna=$id")->row_array();
        $data['data'] = $this->db->query("SELECT n.*, p.id_lijo FROM $this->table n 
        JOIN pemesanan p ON n.id_pemesanan=p.id
        JOIN lijo l ON p.id_lijo=l.id
         WHERE l.id_pengguna='$id_pengguna' AND n.penerima=1 ORDER BY n.created_at DESC")->result_array();
        echo json_encode($data);
    }
}