<?php 
class Lijo extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->table = "lijo";
    }
    public function data(){
        $cari = $this->input->get("cari");
        $having = "GROUP BY l.id ";
        if($cari){
            $cari = "LIKE  '%$cari%'";
            $having .="HAVING rute $cari || l.nama $cari || tipe_lijo $cari";
        }
        if(isset($_POST['filter'])){
            $d = $_POST;
            if($d['filter'] == 1){
                $rute_lijo = $d['rute_lijo'];
                $tipe_lijo = $d['tipe_lijo'];
                if($rute_lijo !='' || $tipe_lijo != ''){
                    $having .="HAVING ";
                }
                if($rute_lijo != ''){
                    $having.=" rute like '%$rute_lijo%' ";
                }
                if($tipe_lijo != ''){
                    if($rute_lijo !=''){
                        $having.=" AND ";
                    }
                    $having.=" tipe_lijo like '%$tipe_lijo%' ";
                }
                
            }
        }
        $data = $this->db->query("SELECT l.id as id_lijo,l.foto,
        (CASE tipe WHEN  1 THEN 'Keliling' WHEN 2 THEN 'Mangkal' WHEN 3 THEN 'Campur' END) as tipe_lijo, 
        l.nama as nama_lijo, (CASE active WHEN 1 THEN 'Active' WHEN 0 THEN 'NonActive' END ) as status, 
        GROUP_CONCAT(r.nama_rute separator ',') rute, GROUP_CONCAT(r.id separator ',') rute_id
         FROM $this->table l LEFT JOIN rute_lijo rj ON l.id=rj.id_lijo LEFT JOIN rute r ON rj.id_rute=r.id $having")->result_array();
        echo json_encode(['data' => $data]);
    }
    public function detail($id){
        $where = " WHERE p.id_lijo='$id' GROUP BY id_produk";
        $d = $_POST;
        if($d){
            if(isset($d['kategori'])){
                $kategori = $d['kategori'];
                if($kategori != "null"){
                    $where.=" HAVING  kategori like '%$kategori%'";
                }
            }
        }
        $data['lijo'] = $this->db->query("SELECT l.foto as foto_lijo, a.alamat, l.id as id_lijo, 
        (CASE tipe WHEN  1 THEN 'Keliling' WHEN 2 THEN 'Mangkal' WHEN 3 THEN 'Campur' END) as tipe_lijo, l.nama as nama_lijo, (CASE active WHEN 1 THEN 'Active' WHEN 0 THEN 'NonActive' END ) as status, p.*, GROUP_CONCAT(r.nama_rute separator ',') rute
         FROM $this->table l 
         LEFT JOIN pengguna p ON l.id_pengguna=p.id 
         LEFT JOIN rute_lijo rj ON l.id=rj.id_lijo 
         LEFT JOIN (SELECT * FROM alamat LIMIT 1 )a ON p.id = a.id_pengguna 
         JOIN rute r ON rj.id_rute=r.id 
         WHERE l.id='$id' GROUP BY l.id")->row_array();
        $data['produk'] = $this->db->query("SELECT k.nama, p.deskripsi, kat.nama as kategori, satuan,
        (CASE WHEN p.foto IS NULL THEN k.foto ELSE p.foto END) as foto,
        harga_satuan, id_produk FROM produk p 
        JOIN (SELECT dp.id_produk, s.nama as satuan, dp.harga as harga_satuan FROM detail_produk dp JOIN satuan s ON dp.id_satuan=s.id) a ON p.id=a.id_produk
        JOIN komoditas k ON p.id_komoditas=k.id 
        JOIN kategori kat ON k.id_kategori=kat.id
        $where ")->result_array();
         echo json_encode($data);
    }
}
