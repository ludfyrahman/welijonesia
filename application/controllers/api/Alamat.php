<?php
class Alamat extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->table = "alamat";
    }
    public function data($id){
        $data['data'] = $this->db->query("SELECT a.*, p.telepon FROM alamat a 
        LEFT JOIN desa d ON a.id_desa=d.id 
        JOIN pengguna p ON a.id_pengguna=p.id
        WHERE a.id_pengguna='$id'")->result_array();
        echo json_encode($data);
    }
    public function kecamatan(){
        $data['data'] = $this->db->get_where("kecamatan", ['kabupaten_id' => '3509'])->result_array();
        echo json_encode($data);
    }
    public function kecamatan_detail($name){
        $data['data'] = $this->db->get_where("kecamatan", ['name' => $name])->row_array();
        echo json_encode($data);
    }
    public function desa_by_kecamatan($id){
        $data['data'] = $this->db->get_where("desa", ['kecamatan_id' => $id])->result_array();
        echo json_encode($data);
    }
    public function desa_detail($name){
        $data['data'] = $this->db->get_where("desa", ['name' => $name])->row_array();
        echo json_encode($data);
    }
    public function add($id){
        $response = [];
        $response['status'] = false;
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $d = $_POST;
            $arr = [
                'id_pengguna' => $id,
                // 'id_desa' => $d['id_desa'],
                'alamat' => $d['alamat']
            ];
            if($d['status']){
                $arr['status'] = 1;
                $this->db->update("$this->table", ['status' => 0], ['id_pengguna' => $id]);
            }
            $this->db->insert("$this->table", $arr);
            $response['pesan'] = 'Berhasil Simpan Alamat';
            $response['status'] = true;
        }else{
            $response['pesan'] = 'Method Not allowed';
            $response['status'] = false;
        }
        echo json_encode($response);
    }
}
