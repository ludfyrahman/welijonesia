<?php 
class Keranjang extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->table = "keranjang";
    }
    public function tambah($id_pengguna){
        $response = [];
        $response['status'] = false;
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $d = $_POST;
            $id_lijo = $d['id_lijo'];
            $id_detail = $d['id_detail_produk'];
            $data = $this->db->get_where("detail_produk", ['id' => $id_detail])->row_array();
            $lijo = $this->db->get_where('keranjang', ['id_pengguna' => $id_pengguna])->row_array();
            $Jlijo = $this->db->get_where('keranjang', ['id_pengguna' => $id_pengguna])->num_rows();
            $keranjang = $this->db->get_where("keranjang", ['id_pengguna' => $id_pengguna, 'id_detail_produk' => $id_detail])->result_array();
            if($Jlijo > 0){
                if($id_lijo != $lijo['id_lijo']){
                    $response['pesan'] = 'Silahkan Kosongi Keranjang anda terlebih dahulu';
               }else{
                $arr = [
                    'id_detail_produk' => $id_detail,
                    'jumlah' => $d['jumlah'],
                    'id_lijo' => $id_lijo,
                    'subtotal'=> $data['harga'] * $d['jumlah'],
                    'created_by' => $id_pengguna,
                    'id_pengguna' => $id_pengguna,
                ];
                    if(count($keranjang) > 0){
                        $jumlah = $d['jumlah'] + $keranjang[0]['jumlah'];
                        $arr = [
                            'jumlah' => $jumlah,
                            'subtotal'=> $data['harga'] * $jumlah,
                        ];
                        $this->db->update("keranjang", $arr, ['id_pengguna' => $id_pengguna, 'id_detail_produk' => $id_detail]);
                        $response['status'] = true;
                    }else{
                        
                        $response['status'] = true;
                        $this->db->insert($this->table, $arr);
                    }
                    $response['pesan'] = "Berhasil Menambahkan ke keranjang";
               }
            }else{
                $arr = [
                    'id_detail_produk' => $id_detail,
                    'jumlah' => $d['jumlah'],
                    'id_lijo' => $id_lijo,
                    'subtotal'=> $data['harga'] * $d['jumlah'],
                    'created_by' => $id_pengguna,
                    'id_pengguna' => $id_pengguna,
                ];
                    if(count($keranjang) > 0){
                        $jumlah = $d['jumlah'] + $keranjang[0]['jumlah'];
                        $arr = [
                            'jumlah' => $jumlah,
                            'subtotal'=> $data['harga'] * $jumlah,
                        ];
                        $this->db->update("keranjang", $arr, ['id_pengguna' => $id_pengguna, 'id_detail_produk' => $id_detail]);
                        $response['status'] = true;
                    }else{
                        
                        $response['status'] = true;
                        $this->db->insert($this->table, $arr);
                    }
                    $response['pesan'] = "Berhasil Menambahkan ke keranjang";
            }
           
        }else{
            $response['pesan'] = 'Method Not allowed';
            $response['status'] = false;
        }
        echo json_encode($response);
    }
    public function data($id){
        $data['lijo'] = $this->db->query("SELECT l.* FROM $this->table k LEFT JOIN lijo l ON k.id_lijo=l.id  WHERE k.id_pengguna=$id")->row_array();
        $data['pesanan'] = $this->db->query("SELECT kom.nama, dp.harga, s.nama as satuan, k.id, k.jumlah, k.subtotal, (CASE WHEN p.foto IS NULL THEN kom.foto ELSE p.foto END) as foto  FROM keranjang k 
        JOIN detail_produk dp ON k.id_detail_produk=dp.id
        JOIN produk p ON dp.id_produk=p.id 
        JOIN komoditas kom ON p.id_komoditas=kom.id
        JOIN satuan s ON dp.id_satuan=s.id
        WHERE k.id_pengguna=$id")->result_array();
        echo json_encode($data);
    }
    public function hapus($id){
        $q = $this->db->delete($this->table, ['id' => $id]);
        if($q){
            $response['pesan'] = 'Berhasil Hapus data';
            $response['status'] = true;
        }else{
            $response['pesan'] = 'Gagal Hapus data';
            $response['status'] = false;
        }
        echo json_encode($response);
    }
}
