<?php 
class Produk extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->table = "produk";
    }
    public function detail($id){
        $data['data'] = $this->db->query("SELECT k.nama, p.deskripsi, 
        (CASE WHEN p.foto IS NULL THEN k.foto ELSE p.foto END) as foto ,
        qu.harga,satuan,
        kat.nama as kategori
        FROM produk p JOIN komoditas k ON p.id_komoditas=k.id 
        JOIN kategori kat ON k.id_kategori=kat.id
        JOIN (SELECT dp.id_produk, dp.harga, s.nama as satuan FROM detail_produk dp JOIN satuan s ON dp.id_satuan=s.id ) qu ON p.id=qu.id_produk
        WHERE p.id='$id'")->row_array();
        echo json_encode($data);
    }
    public function detail_produk($id){
        $data['data'] = $this->db->query("SELECT dp.*, s.nama FROM detail_produk dp JOIN satuan s ON dp.id_satuan=s.id WHERE dp.id_produk='$id'")->result_array();
        if(count($data['data']) > 0){
            $data['id_lijo'] = $this->db->get_where("produk", ['id' => $data['data'][0]['id_produk']])->row_array()['id_lijo'];   
        }
        echo json_encode($data);
    }
    // data produk untuk lijo
    public function data($id_lijo){
        $cari = $this->input->get("cari");
        $where = " WHERE l.id_pengguna=$id_lijo ";
        if($cari){
            $cari = "LIKE  '%$cari%'";
            $where .=" AND (k.nama $cari || kat.nama $cari)";
        }
        $data['data'] = $this->db->query("SELECT k.nama, p.deskripsi, p.id, 
        (CASE WHEN p.foto IS NULL THEN k.foto ELSE p.foto END) as foto ,
        kat.nama as kategori
        FROM produk p JOIN komoditas k ON p.id_komoditas=k.id
        JOIN kategori kat ON k.id_kategori=kat.id 
        JOIN lijo l ON p.id_lijo=l.id
        JOIN pengguna pg ON pg.id=l.id_pengguna
        
        $where")->result_array();
        echo json_encode($data);
    }
    public function komoditas($id_lijo){
        $cari = $this->input->get("cari");
        $where = " ";
        if($cari){
            $cari = "LIKE  '%$cari%'";
            $where =" WHERE (k.nama $cari || kat.nama $cari)";
        }
        $komoditas= $this->db->query("SELECT k.*, kat.nama as kategori FROM `komoditas` k 
        JOIN kategori kat ON k.id_kategori=kat.id 
        $where")->result_array();
        $produk = $this->db->query("SELECT p.id_komoditas FROM produk p 
        JOIN lijo l ON p.id_lijo=l.id
        WHERE l.id_pengguna=$id_lijo ")->result_array();
        $dd = [];
        foreach ($produk as $p) {
            $dd[] = $p['id_komoditas'];
        }
        // print_r($dd);
        $data['data'] = [];
        foreach ($komoditas as $k) {
            // echo "Berhasil ".$k['id'];
            if(!in_array($k['id'], $dd)){
                $data['data'][] = $k;
            }
        }
        echo json_encode($data);
    }
    public function detail_komoditas($id){
        $data['data'] = $this->db->query("SELECT k.*, kat.nama as kategori FROM `komoditas` k JOIN kategori kat ON k.id_kategori=kat.id WHERE k.id='$id'")->row_array();
        echo json_encode($data);
    }
    public function satuan(){
        $data['data'] = $this->db->get("satuan")->result_array();
        echo json_encode($data);
    }
    public function simpan(){
        $response = [];
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $d = $_POST;
            $f = $_FILES;
            $lijo = $this->db->get_where("lijo", ['id_pengguna' => $d['id_pengguna']])->row_array();
            $response['status'] = false;
            try {
                $komoditas = $this->db->get_where("komoditas", ['id' => $d['id_komoditas']])->row_array();
                $arr = ['id_komoditas' => $d['id_komoditas'], 'deskripsi' => $d['deskripsi'], 'id_lijo' => $lijo['id'], 'created_by' => $d['id_pengguna']];
                if(isset($f['file']['name'])){
                    Response_Helper::UploadImage($f['file'], 'produk');
                    $arr['foto'] = $f['file']['name'];
                }else{
                    $arr['foto'] = $komoditas['foto'];
                }
                $this->db->insert("produk", $arr);
                $id_produk = $this->db->insert_id();
                for ($i=1; $i <= 4; $i++) { 
                    if(isset($d['harga'.$i])){
                        if($d['satuan'.$i] != ''){
                            $satuan = $this->db->get_where("satuan", ['nama' => $d['satuan'.$i]])->row_array();
                            $this->db->insert("detail_produk", ['id_produk' => $id_produk, 'id_satuan' => $satuan['id'], 'jumlah' => 0, 'harga' => $d['harga'.$i]]);
                        }
                    }
                }
                if($d['harga1']!=''){
                    $response['status'] = true;
                    $response['message'] = 'Berhasil Simpan ';
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Silahkan Lengkapi isi form dengan benar';
                }
            }
            catch(Exception $e) {
                // $this->login();
                $response['message'] = 'Kesalahan';
            }
        }else{
            $response['message'] = 'Method Not allowed';
        }
        echo json_encode($response);
    }
    public function update($id){
        $response = [];
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $d = $_POST;
            $f = $_FILES;
            $lijo = $this->db->get_where("lijo", ['id_pengguna' => $d['id_pengguna']])->row_array();
            $response['status'] = false;
            try {
                $komoditas = $this->db->get_where("komoditas", ['id' => $d['id_komoditas']])->row_array();
                $arr = ['id_komoditas' => $d['id_komoditas'], 'deskripsi' => $d['deskripsi'], 'id_lijo' => $lijo['id'], 'created_by' => $d['id_pengguna']];
                if(isset($f['file']['name'])){
                    Response_Helper::UploadImage($f['file'], 'produk');
                    $arr['foto'] = $f['file']['name'];
                }else{
                    $arr['foto'] = $komoditas['foto'];
                }
                // $this->db->insert("produk", $arr);
                $this->db->update("produk", $arr, ['id' => $id]);
                $id_produk = $id;
                if(isset($d['harga1'])){
                    if($d['satuan1'] != ''){
                        $this->db->delete("detail_produk", ['id_produk' => $id]);
                    }
                }
                for ($i=1; $i <= 4; $i++) { 
                    if(isset($d['harga'.$i])){
                        if($d['satuan'.$i] != ''){
                            $satuan = $this->db->get_where("satuan", ['nama' => $d['satuan'.$i]])->row_array();
                            $this->db->insert("detail_produk", ['id_produk' => $id_produk, 'id_satuan' => $satuan['id'], 'jumlah' => 0, 'harga' => $d['harga'.$i]]);
                        }
                    }
                }
                if($d['harga1']!=''){
                    $response['status'] = true;
                    $response['message'] = 'Berhasil Update Produk ';
                }else{
                    $response['status'] = false;
                    $response['message'] = 'Silahkan Lengkapi isi form dengan benar';
                }
            }
            catch(Exception $e) {
                // $this->login();
                $response['message'] = 'Kesalahan';
            }
        }else{
            $response['message'] = 'Method Not allowed';
        }
        echo json_encode($response);
    }
    public function edit($id){
        $response = [];
        $data = $this->db->query("SELECT p.*, k.nama, kat.nama as kategori FROM produk p 
        JOIN komoditas k ON p.id_komoditas=p.id 
        JOIN kategori kat ON k.id_kategori=kat.id
        WHERE p.id=$id")->row_array();
        $detail = $this->db->query("SELECT dp.*, s.nama FROM detail_produk dp JOIN satuan s ON dp.id_satuan=s.id WHERE id_produk = $data[id]")->result_array();
        $response['data'] = $data;
        $response['detail'] = $detail;
        echo json_encode($response);
    }
}


?>