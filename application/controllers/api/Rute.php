<?php
class Rute extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->table = "rute";
    }
    public function data(){
        $data['data'] = $this->db->get("rute")->result_array();
        echo json_encode($data);
    }
    public function get_id(){
        $name = $_POST['name'];
        $data['data'] = $this->db->get_where("rute",['nama_rute' => $name])->row_array();
        echo json_encode($data);
    }
}
