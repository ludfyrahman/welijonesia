<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Lijo extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        if (!is_logged()) {
            return redirect(base_url('admin/auth'));
        }
        $this->load->model('M_lijo', 'lijo');
        $this->load->model('M_customer', 'pengguna');
    }
    public function index()
    {
        $modelLijo =  [];
        $lijo = $this->lijo->getAll();
        foreach ($lijo as $index => $value) {
            $modelLijo[$index]['id'] = $value->id;
            $modelLijo[$index]['nama'] = $value->nama;
            $modelLijo[$index]['foto'] = $value->foto;
            $modelLijo[$index]['tipe'] = $value->tipe;
            $modelLijo[$index]['rate'] = $value->rate;
            $modelLijo[$index]['like'] = $value->like;
            $modelLijo[$index]['active'] = $value->active;
            $modelLijo[$index]['created_at'] = $value->created_at;

            //Get Kategori where komoditas.id_kategori same with kategori.id
            $kategori = $this->db->where('id', $value->id_pengguna)->get('pengguna')->row_array();
            $modelLijo[$index]['pengguna'] = $kategori['nama'] ?? '';
        }

        $data = [
            'title' => 'Lijo',
            'model' =>  $modelLijo,
            'page' => 'lijo'
        ];
        // print_r($modelKomoditas);
        $this->load->view('containers/app', $data);
    }

    public function tambah()
    {

        $pengguna = $this->pengguna->getAll();

        if (!$_POST) {
            $request    = [
                'nama'        => '',
                'pengguna'        => '',
                'tipe'        => '',
            ];
        } else {
            $request    = $this->input->post(null, true);
            return var_dump($request);
            $validationRules = [
                [
                    'field'    => 'nama',
                    'label'    => 'Nama',
                    'rules'    => 'trim|required'
                ],
                [
                    'field'    => 'pengguna',
                    'label'    => 'Pengguna',
                    'rules'    => 'trim|required'
                ],
                [
                    'field'    => 'tipe',
                    'label'    => 'Tipe',
                    'rules'    => 'trim|required'
                ],
            ];


            if (validator($validationRules)) {
                $data = [
                    'nama' => $request['nama'],
                    'id_pengguna' => $request['pengguna'],
                    // 'kuantitas' => $request['kuantitas'],
                    'tipe' => $request['tipe'],
                    'rate' => 0,
                    'like' => 0,
                    'active' => 1,
                ];

                if ($this->lijo->create($data)) {
                    $this->session->set_flashdata('success', $request['nama'] . ' Berhasil ditambahkan!');
                    return redirect(base_url(uri_string()));
                }
                $this->session->set_flashdata('error', 'Terjadi kesalahan saat menambahkan lijo, Periksa kembali!');
            }
        }

        $data = [
            'title' => 'Lijo',
            'input' => $request,
            'data' => [
                'pengguna' => $pengguna,
            ],
            'page' => 'tambah_lijo'
        ];

        $this->load->view('containers/app', $data);
    }


    public function upload_photo()
    {
        // Set preference
        $config['upload_path'] = 'upload/img';
        $config['allowed_types'] = 'jpg|gif|png|jpeg|JPG|PNG';
        $config['max_size'] = '5000'; // max_size in kb
        $config['file_name'] = md5($_FILES['photo']['name']);

        //Load upload library
        $this->load->library('upload', $config);

        // File upload
        if ($this->upload->do_upload('photo')) {
            // Get data about the file
            $uploadData = $this->upload->data();
            $filename = $uploadData['file_name'];

            // Initialize array
            $data['filename'] = $filename;
        }
        return $data;
    }
}

/* End of file Komoditas.php */
