<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Komoditas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        if (!is_logged()) {
            return redirect(base_url('admin/auth'));
        }
        $this->load->model('M_komoditas', 'komoditas');
        $this->load->model('M_kategori', 'kategori');
        $this->load->model('M_satuan', 'satuan');
    }
    public function index()
    {
        $komoditas = $this->db->select('komoditas.id,komoditas.nama,komoditas.foto,komoditas.harga,kategori.nama as kategori,satuan.nama as satuan')
            ->join('kategori', 'komoditas.id_kategori=kategori.id')->join('satuan', 'komoditas.id_satuan=satuan.id')
            ->get('komoditas')->result_array();
        $data = [
            'title' => 'Komoditas',
            'model' => $komoditas,
            'page' => 'komoditas'
        ];
        // print_r($modelKomoditas);
        $this->load->view('containers/app', $data);
    }

    public function tambah()
    {

        $kategori = $this->kategori->getAll();
        $satuan = $this->satuan->getAll();

        if (!$_POST) {
            $request    = [
                'nama'        => '',
                'kategori'        => '',
                'kuantitas'        => '',
                'satuan'        => '',
                'harga'        => '',
            ];
        } else {
            $request    = $this->input->post(null, true);

            $validationRules = [
                [
                    'field'    => 'nama',
                    'label'    => 'Nama',
                    'rules'    => 'trim|required'
                ],
                [
                    'field'    => 'kategori',
                    'label'    => 'Kategori',
                    'rules'    => 'trim|required'
                ],
                [
                    'field'    => 'kuantitas',
                    'label'    => 'Kuantitas',
                    'rules'    => 'trim|required'
                ],
                [
                    'field'    => 'satuan',
                    'label'    => 'Satuan',
                    'rules'    => 'trim|required'
                ],
                [
                    'field'    => 'harga',
                    'label'    => 'Harga',
                    'rules'    => 'trim|required'
                ]
            ];

            if (validator($validationRules)) {
                $data = [
                    'nama' => $request['nama'],
                    'id_kategori' => $request['kategori'],
                    // 'kuantitas' => $request['kuantitas'],
                    'id_satuan' => $request['satuan'],
                    'harga' => $request['harga'],
                ];

                if ($this->komoditas->create($data)) {
                    $this->session->set_flashdata('success', $request['nama'] . ' Berhasil ditambahkan!');
                    return redirect(base_url(uri_string()));
                }
                $this->session->set_flashdata('error', 'Terjadi kesalahan saat menambahkan komoditas, Periksa kembali!');
            }
        }

        $data = [
            'title' => 'Komoditas',
            'input' => $request,
            'data' => [
                'kategori' => $kategori,
                'satuan' => $satuan,
            ],
            'page' => 'tambah_komoditas'
        ];

        $this->load->view('containers/app', $data);
    }
}

/* End of file Komoditas.php */
