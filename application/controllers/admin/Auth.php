<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('M_customer', 'pengguna');
    }

    public function index()
    {
        if (is_logged()) {
            return redirect(base_url('admin/dashboard'));
        }

        if (!$_POST) {
            $request    = [
                'email'        => '',
                'password'    => '',
            ];
        } else {
            $request    = $this->input->post(null, true);

            $validationRules = [
                [
                    'field'    => 'email',
                    'label'    => 'Email',
                    'rules'    => 'trim|required'
                ],
                [
                    'field'    => 'password',
                    'label'    => 'Password',
                    'rules'    => 'trim|required'
                ]
            ];

            if (validator($validationRules)) {
                $pengguna = $this->db->where('email', $request['email'])->get('pengguna')->row_array();

                if (password_verify($request['password'], $pengguna['password'])) {

                    if ($pengguna['level'] != 1) {
                        $this->session->set_flashdata('warning', 'Maaf anda tidak memiliki akses!');
                    } else {
                        $sess_data = [
                            'userId'        => $pengguna['id'],
                            'userName'        => $pengguna['nama'],
                            'userEmail'        => $pengguna['email'],
                            'level'        => $pengguna['level'],
                            'id_logged'    => true,
                        ];
                        $this->session->set_userdata($sess_data);

                        $this->session->set_flashdata('success', 'Berhasil melakukan login!');
                        return redirect(base_url('admin/dashboard'));
                    }
                } else {
                    $this->session->set_flashdata('error', 'E-Mail atau Password salah atau akun Anda sedang tidak aktif!');
                }
            }
        }

        $data = [
            'title' => 'Auth',
            'input' => $request,
            'page' => 'auth'
        ];
        $this->load->view('containers/auth', $data);
    }

    public function logout()
    {
        if (!is_logged()) {
            return redirect(base_url('admin/auth'));
        }

        $sess_data = [
            'userId', 'userName', 'userEmail', 'level', 'is_logged'
        ];

        $this->session->unset_userdata($sess_data);
        $this->session->sess_destroy();
        redirect(base_url('admin/auth'));
    }
}

/* End of file Auth.php */
