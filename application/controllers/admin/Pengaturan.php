<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Pengaturan extends CI_Controller
{

    public function index()
    {
        $data = [
            'title' => 'Pengaturan',
            'page' => 'pengaturan'
        ];

        $this->load->view('containers/app', $data);
    }
}

/* End of file Pengaturan.php */
