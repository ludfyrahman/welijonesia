<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Pengguna extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        if (!is_logged()) {
            return redirect(base_url('admin/auth'));
        }
        $this->load->model('M_customer', 'pengguna');
    }

    public function index()
    {
        $pengguna = $this->pengguna->getAll();

        $data = [
            'title' => 'Pengguna',
            'model' => $pengguna,
            'page' => 'pengguna'
        ];
        $this->load->view('containers/app', $data);
    }

    public function tambah()
    {
        if (!$_POST) {
            $request    = [
                'nama'        => '',
                'telepon'        => '',
                'whatsapp'        => '',
                'jenis_kelamin'        => '',
                'level'        => '',
                'email'        => '',
                'password'    => '',
                'konfirmasi_password'    => '',
            ];
        } else {
            $request    = $this->input->post(null, true);

            $validationRules = [
                [
                    'field'    => 'nama',
                    'label'    => 'Nama',
                    'rules'    => 'trim|required'
                ],
                [
                    'field'    => 'email',
                    'label'    => 'Email',
                    'rules'    => 'trim|required|valid_email'
                ],
                [
                    'field'    => 'telepon',
                    'label'    => 'Telepon',
                    'rules'    => 'trim|required|min_length[9]'
                ],
                [
                    'field'    => 'whatsapp',
                    'label'    => 'Whastapp',
                    'rules'    => 'trim|min_length[9]'
                ],
                [
                    'field'    => 'jenis_kelamin',
                    'label'    => 'Jenis Kelamin',
                    'rules'    => 'trim|required'
                ],
                [
                    'field'    => 'level',
                    'label'    => 'Level',
                    'rules'    => 'trim|required'
                ],
                [
                    'field'    => 'password',
                    'label'    => 'Password',
                    'rules'    => 'trim|required'
                ],
                [
                    'field'    => 'konfirmasi_password',
                    'label'    => 'Konfirmasi Password',
                    'rules'    => 'trim|required|matches[password]'
                ]
            ];

            if (validator($validationRules)) {
                $data = [
                    'nama' => $request['nama'],
                    'email' => strtolower($request['email']),
                    'telepon' => $request['telepon'],
                    'wa' => $request['whastapp'] ?? '',
                    'jenis_kelamin' => $request['jenis_kelamin'],
                    'level' => $request['level'],
                    'password' => password_hash($request['password'], PASSWORD_BCRYPT),
                ];

                if ($this->pengguna->create($data)) {
                    $this->session->set_flashdata('success', $request['nama'] . ' Berhasil ditambahkan!');
                    return redirect(base_url(uri_string()));
                }
                $this->session->set_flashdata('error', 'Terjadi kesalahan saat menambahkan pengguna, Periksa kembali!');
            }
        }

        $data = [
            'title' => 'Pengguna',
            'input' => $request,
            'page' => 'tambah_pengguna'
        ];
        $this->load->view('containers/app', $data);
    }

    public function hapus($id)
    {
        if ($this->pengguna->destroy($id)) {
            echo json_encode([
                'status' => 1,
                'message' => 'Pengguna berhasil dihapus'
            ]);
            return;
        }
        echo json_encode([
            'status' => 0,
            'message' => 'Pengguna gagal dihapus'
        ]);
        return;
    }
}

/* End of file Pengguna.php */
