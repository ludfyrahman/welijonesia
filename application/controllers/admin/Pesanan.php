<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Pesanan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        if (!is_logged()) {
            return redirect(base_url('admin/auth'));
        }
        $this->load->model('M_pemesanan', 'pesanan');
    }

    public function index()
    {
        $modelPesanan =  [];

        $pesanan = $this->db->select_sum('detail_pemesanan.subtotal')->select('pemesanan.id,pengguna.nama as pemesan,pemesanan.tanggal_pemesanan,pemesanan.tanggal_expired,lijo.nama as lijo,alamat.alamat,pemesanan.status')
            ->group_by("pemesanan.id")
            ->join('pengguna', 'pemesanan.id_pengguna=pengguna.id')
            ->join('alamat', 'pemesanan.id_alamat=alamat.id')
            ->join('lijo', 'pemesanan.id_lijo=lijo.id')
            ->join('detail_pemesanan', 'pemesanan.id=detail_pemesanan.id_pemesanan')
            ->get('pemesanan')->result_array();

        $data = [
            'title' => 'Pesanan',
            'model' => $pesanan,
            'page' => 'pesanan'
        ];
        $this->load->view('containers/app', $data);
    }

    public function tambah()
    {
        $data = [
            'title' => 'Pesanan',
            'page' => 'tambah_pesanan'
        ];
        $this->load->view('containers/app', $data);
    }
}

/* End of file Pesanan.php */
